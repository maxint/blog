# This is a personal blog system

This is a personal blog system at google appengine [maxint.tk](http://www.maxint.tk).

The theme template is basically based on [hzqtc's github blog](https://github.com/hzqtc/hzqtc.github.com).

## License

All other directories and files are MIT Licensed. Feel free to use the HTML and CSS as you please.
