package lnyblog

import (
    "appengine"
    "io/ioutil"
    "os"
    "regexp"
    "strconv"
    "time"
)

var (
    blogDir = "_blog/"
    regBlogItems = regexp.MustCompile(`<li[^>]*><a[^>]*href\s?=\s?"(?P<url>[^"]+?)">(?P<y>\d+)年(?P<m>\d+)月(?P<d>\d+)日\s(?P<h>\d+)时(?P<m>\d+)分\s发布\s(?P<title>[^<]+)</a></li>`)
    regBlogID = regexp.MustCompile(`\./blog/(\d+).html`)
    regBlogBody = regexp.MustCompile(`(?sm)</h1><div>(.*)</div></body>`)
)

func importBlogList(c appengine.Context, name string) (err error) {
    f, err := os.Open(name)
    if err != nil { return }
    buf, err := ioutil.ReadAll(f)
    if err != nil { return }
    matchs := regBlogItems.FindAllStringSubmatch(string(buf), 70)
    var bf *os.File
    for i, m := range matchs {
        path := m[1]
        year, _ := strconv.Atoi(m[2])
        mouth, _ := strconv.Atoi(m[3])
        day, _ := strconv.Atoi(m[4])
        hour, _ := strconv.Atoi(m[5])
        minute, _ := strconv.Atoi(m[6])
        title := m[7]
        date := time.Date(year, time.Month(mouth), day, hour, minute, 0, 0, localTimeZone)

        bf, err = os.Open(blogDir + path)
        if err != nil { return }
        buf, err = ioutil.ReadAll(bf)
        if err != nil { return }
        bm := regBlogBody.FindStringSubmatch(string(buf))

        c.Debugf("%v, %v", i, path)
        if i == 3 {
            c.Debugf("%v", bm)
        }

        ph := &PostHeader {
            Title: title,
            CreatedAt: date,
            UpdatedAt: date,
            PermaLink: regBlogID.FindStringSubmatch(path)[1],
        }
        err = putPost(c, ph, bm[1])
        if err != nil { return }
    }
    return
}
