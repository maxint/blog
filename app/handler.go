package lnyblog

import (
	"appengine"
	"errors"
	"net/http"
)

var (
	Err404                = errors.New("Required URL does not existed")
	ErrEmptySite          = errors.New("Empty site")
	ErrBlogNotExisted     = errors.New("Required blog does not existed")
	ErrInvalidDate        = errors.New("Required date is invalid")
	ErrArchiveNotExisted  = errors.New("Required archive does not existed")
	ErrCategoryNotExisted = errors.New("Required category does not existed")
	ErrTagNotExisted      = errors.New("Required tag does not existed")
	ErrPageOutOfRange     = errors.New("Required page is out of range")
	ErrInvalidPage        = errors.New("Required page is invalid")
	ErrNoFileUploaded     = errors.New("No file is uploaded")
	ErrFileNotExisted     = errors.New("Required file does not existed")
)

// Error handlers
func serveError(c appengine.Context, w http.ResponseWriter, err error) {
	//http.Error(w, err.Error(), http.StatusInternalServerError)
	layoutView(c, w, "error404", err.Error(), err.Error())
    c.Errorf("%v", err.Error())
}

// Server handler management
func getErrorHandler(handler (func(appengine.Context, http.ResponseWriter, *http.Request) error)) func(http.ResponseWriter, *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		c := appengine.NewContext(r)
		var err error
		defer func() {
			if err != nil {
				serveError(c, w, err)
			}
		}()
		err = handler(c, w, r)
	}
}

func addHandler(pattern string, handler (func(appengine.Context, http.ResponseWriter, *http.Request) error)) {
	http.HandleFunc(pattern, getErrorHandler(handler))
}

// Server handler management
func getLogHandler(handler (func(appengine.Context, http.ResponseWriter, *http.Request) error)) func(http.ResponseWriter, *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		c := appengine.NewContext(r)
		var err error
		defer func() {
			if err != nil {
				c.Errorf("%v", err.Error())
			}
		}()
		err = handler(c, w, r)
	}
}

func addLogHandler(pattern string, handler (func(appengine.Context, http.ResponseWriter, *http.Request) error)) {
	http.HandleFunc(pattern, getLogHandler(handler))
}
