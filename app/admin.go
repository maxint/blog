package lnyblog

import (
	"appengine"
	"appengine/blobstore"
	"appengine/memcache"
	"html/template"
	"net/http"
	"net/url"
	"path"
	"strings"
	"time"
)

var (
	admTemplates = map[string]*template.Template{}
)

func initAdminTemplate() {
	addAdminTemplate("create", "base", "editor")
	addAdminTemplate("modify", "base", "editor")
	addAdminTemplate("index", "base")
}

func addAdminTemplate(names ...string) {
	admTemplates[names[0]] = getTemplate("views/admin/", names...)
}

func redirectToAdminRoot(w http.ResponseWriter, r *http.Request, err error) error {
	if err == nil {
		http.Redirect(w, r, "/admin", http.StatusFound)
	}
	return err
}

func getSplitTags(s string) []string {
	intags := strings.Split(s, ",")
	tags := make([]string, 0, len(intags))
	for _, v := range intags {
		v = strings.Trim(v, " ")
		if v != "" {
			tags = append(tags, v)
		}
	}
	return tags
}

//-----------------------------------------------------------------------------
// Handlers

// Admin root
func adminRootHandler(c appengine.Context, w http.ResponseWriter, r *http.Request) (err error) {
	headers, _, err := getHeadersInRange(c, 0, -1)
	if err != nil {
		return
	}
	uploadURL, err := blobstore.UploadURL(c, "/admin/restore/", nil)
	if err != nil {
		return
	}
	data := &struct {
		Headers   []*PostHeader
		UpdateURL *url.URL
	}{
		headers,
		uploadURL,
	}
	return admTemplates["index"].Execute(w, data)
}

// Flush memcache
func flushCacheHandler(c appengine.Context, w http.ResponseWriter, r *http.Request) (err error) {
	err = memcache.Flush(c)
	return redirectToAdminRoot(w, r, err)
}

// Create post
func postCreateHandler(c appengine.Context, w http.ResponseWriter, r *http.Request) error {
	return admTemplates["create"].Execute(w, nil)
}

func postSaveHandler(c appengine.Context, w http.ResponseWriter, r *http.Request) (err error) {
	date := time.Now().In(localTimeZone)
	permalink := createPermaLink(date, r.FormValue("permalink"))
	header := PostHeader{
		permalink,
		r.FormValue("title"),
		date,
		date,
		r.FormValue("category"),
		getSplitTags(r.FormValue("tags")),
		nil,
	}
	err = putPost(c, &header, r.FormValue("content"))
	if err != nil {
		return
	}
	http.Redirect(w, r, "/blog/"+permalink, http.StatusFound)
	return nil
}

// Modify post
func postModifyHandler(c appengine.Context, w http.ResponseWriter, r *http.Request) (err error) {
	permalink := path.Base(r.URL.Path)
	p, err := getPost(c, permalink)
	if err != nil {
		return
	}
	return admTemplates["modify"].Execute(w, p)
}

func postUpdateHandler(c appengine.Context, w http.ResponseWriter, r *http.Request) (err error) {
	createdAt, err := time.Parse(EditorTimeLayout, r.FormValue("createdat"))
	if err != nil {
		return
	}
	updatedAt := time.Now().In(localTimeZone)
	permalink := r.FormValue("permalink")
	header := PostHeader{
		permalink,
		r.FormValue("title"),
		createdAt,
		updatedAt,
		r.FormValue("category"),
		getSplitTags(r.FormValue("tags")),
		nil,
	}
	err = putPost(c, &header, r.FormValue("content"))
	if err != nil {
		return
	}
	http.Redirect(w, r, "/blog/"+permalink, http.StatusFound)
	return nil
}

// Delete post
func postDeleteHandler(c appengine.Context, w http.ResponseWriter, r *http.Request) (err error) {
	err = deletePost(c, r.URL.Path)
	return redirectToAdminRoot(w, r, err)
}

func postDeleteAllHandler(c appengine.Context, w http.ResponseWriter, r *http.Request) (err error) {
	err = deleteAllPosts(c)
	return redirectToAdminRoot(w, r, err)
}

// Save/load post to/from XML.
func saveToXMLHandler(c appengine.Context, w http.ResponseWriter, r *http.Request) (err error) {
	err = saveAllPostsToXML(c)
	return redirectToAdminRoot(w, r, err)
}

func loadFromXMLHandler(c appengine.Context, w http.ResponseWriter, r *http.Request) (err error) {
	err = loadAllPostsFromXML(c)
	return redirectToAdminRoot(w, r, err)
}

// Backup / restore as zip
func backupHandler(c appengine.Context, w http.ResponseWriter, r *http.Request) (err error) {
	// Create blob
	blobw, err := blobstore.Create(c, "application/zip")
	if err != nil { return }

	// Write as ZIP
	err = packPostsAsZip(c, blobw)
	if err != nil { return }

	// Save and get blob key
	err = blobw.Close()
	if err != nil { return }
	k, err := blobw.Key()
	if err != nil { return }

	// Redirect to download url
	http.Redirect(w, r, "/admin/backup/serve/?blobKey="+string(k), http.StatusFound)
	return nil
}

func blobServeHandler(c appengine.Context, w http.ResponseWriter, r *http.Request) (err error) {
	blobstore.Send(w, appengine.BlobKey(r.FormValue("blobKey")))
	return nil
}

func restoreHandler(c appengine.Context, w http.ResponseWriter, r *http.Request) (err error) {
	// Parse upload POST
	blobs, _, err := blobstore.ParseUpload(r)
	if err != nil { return }

	file := blobs["backup"]
	if len(file) == 0 {
		return redirectToAdminRoot(w, r, ErrNoFileUploaded)
	}

	// Get blob key and read
    k := file[0].BlobKey
    blobr := blobstore.NewReader(c, k)
    info, err := blobstore.Stat(c, k)
	if err != nil { return }
	// Read from ZIP
	err = putPostsFromZip(c, blobr, info.Size)

	return redirectToAdminRoot(w, r, err)
}

func importHandler(c appengine.Context, w http.ResponseWriter, r *http.Request) (err error) {
    err = importBlogList(c, blogDir + "blog_index.html")
    return redirectToAdminRoot(w, r, err)
}
