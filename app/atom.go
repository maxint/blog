package lnyblog

import (
    "appengine"
    "encoding/xml"
    "html/template"
    "net/http"
    "time"
)

var (
    atomTemplate = template.Must(template.New("atom.xml").Funcs(
        template.FuncMap{"date_to_xmlschema": func(t time.Time) string { return t.Format(time.RFC3339) }},
        ).ParseFiles("views/atom.xml"),
    )
)

func atomHandler(c appengine.Context, w http.ResponseWriter, r *http.Request) (err error) {
    if !updateResponseHeader(w, r, getLastUpdatedTime(c)) {
        return
    }
    w.Header().Set("Content-Type", "text/xml")
    _, err = w.Write([]byte(xml.Header))
    if err != nil { return }
    // cache
    if err = loadCache(c, w, "Atom"); err != ErrCacheMiss { return }
    // no cache
    posts, err := getLastestNPosts(c, AtomPostCount)
    if err != nil { return }
    updateAt := getLastUpdatedTime(c)
    return createCacheForTemplate(c, w, "Atom", atomTemplate, &struct{
        Posts       []*Post
        UpdatedAt   time.Time
        Host        string
    }{
        posts,
        updateAt,
        "http://" + r.Host,
    })
    return
}
