package lnyblog

import (
    _ "appengine/remote_api"
)

func init() {
    initTemplates()
    initAdminTemplate()

    addHandler("/", rootHandler)
    addHandler("/side/", sideHandler)
    addHandler("/blog/", blogHandler)
    addHandler("/category/", categoryPageHandler)
    addHandler("/tag/", tagPageHandler)
    addHandler("/archive/", archivePageHandler)
    addHandler("/about", aboutHandler)
    addHandler("/atom", atomHandler)
    addHandler("/sitemap.txt", sitemapHandler)

    // Administration handlers
    addHandler("/admin/", adminRootHandler)
    addHandler("/admin/flush", flushCacheHandler)
    addHandler("/admin/post/create", postCreateHandler)
    addHandler("/admin/post/save", postSaveHandler)
    addHandler("/admin/post/modify/", postModifyHandler)
    addHandler("/admin/post/update", postUpdateHandler)
    addHandler("/admin/post/delete/", postDeleteHandler)
    addHandler("/admin/post/clear/", postDeleteAllHandler)

    // Image upload
    addLogHandler("/admin/upload", uploadHandler)
    addHandler("/files/", fileServeHandler)

    // Backup / restore
    addHandler("/admin/backup/serve/", blobServeHandler)
    addHandler("/admin/backup/", backupHandler)
    addHandler("/admin/restore/", restoreHandler)
    addHandler("/admin/import/", importHandler)

    // Development handlers
    addHandler("/admin/savexml", saveToXMLHandler)
    addHandler("/admin/loadxml", loadFromXMLHandler)
}
