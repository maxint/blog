package lnyblog

import (
    "appengine"
    "text/template"
    "net/http"
)

var (
    sitemapTemplate = template.Must(template.ParseFiles("views/sitemap.txt"))
)

func sitemapHandler(c appengine.Context, w http.ResponseWriter, r *http.Request)(err error){
    if !updateResponseHeader(w, r, getLastUpdatedTime(c)) {
        return
    }
    w.Header().Set("Content-Type", "text/plain")
    // cache
    if err = loadCache(c, w, "SiteMap"); err != ErrCacheMiss { return }
    // no cache
    posts, _, err := getHeadersInRange(c, 0, -1)
    if err != nil { return }
    return createCacheForTemplate(c, w, "SiteMap", sitemapTemplate, &struct{
        Posts       []*PostHeader
        Host        string
    }{
        posts,
        "http://" + r.Host,
    })
}
