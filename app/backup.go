package lnyblog

import (
    "appengine"
    "archive/zip"
    "io"
)

func packPostsAsZip(c appengine.Context, w io.Writer) (err error) {
    // Create zip
    zipw := zip.NewWriter(w)

    posts, err := getAllPosts(c)
    if err != nil { return }
    var pw io.Writer
    for _, p := range posts {
        // Create and add a file to the zip
        pw, err = zipw.Create(p.Header.PermaLink + ".xml")
        if err != nil { return }
        err = p.writeXML(pw)
        if err != nil { return }
    }
    // Close finishes writing the zip file by writing the central directory. 
    // It does not (and can not) close the underlying writer. 
    return zipw.Close()
}

func putPostsFromZip(c appengine.Context, ra io.ReaderAt, size int64) (err error) {
    // Create zip
    zipr, err := zip.NewReader(ra, size)
    if err != nil { return }

    var rc io.ReadCloser
    for _, zr := range zipr.File {
        p := new(Post)
        // Create and add a file to the zip
        rc, err = zr.Open()
        if err != nil { return }
        err = p.readXML(rc)
        if err != nil { return }
        err = putPost(c, p.Header, p.Body)
        if err != nil { return }
        err = rc.Close()
        if err != nil { return }
    }
    return
}
