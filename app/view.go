package lnyblog

import (
    "appengine"
    "appengine/user"
    "html/template"
    "net/http"
    "path"
    "strconv"
    "strings"
    "time"
)

//-----------------------------------------------------------------------------
// Templates

var (
    themePath = "themes/" + ThemeName + "/"
    htmlfmtFuncs = template.FuncMap {
        "datefmt": func(t time.Time) string { return t.Format("Jan 2 2006") },
        "fulldatefmt": func(t time.Time) string { return t.Format(EditorTimeLayout) },
        "slicefmt": func(s []string) string { return strings.Join(s, ",") },
    }
    templates = map[string]*template.Template{}
)

func (p *Post) HTML() template.HTML {
    return template.HTML(p.Body)
}

func initTemplates() {
    addTemplate("index", "base", "post")
    addTemplate("blog", "base", "post")
    addTemplate("error404", "base")
    addTemplate("about", "base")
    addTemplate("side")
}

func addTemplate(names ...string) {
    templates[names[0]] = getTemplate(themePath, names...)
}

func getTemplate(path string, names ...string) *template.Template {
    num := len(names)
    filenames := make([]string, num)
    for i, v := range names {
        filenames[i] = path + v + ".html"
    }
    var base string
    if num == 1 {
        base = names[0]
    } else {
        base = names[1]
    }
    return template.Must(template.New(base + ".html").Funcs(htmlfmtFuncs).ParseFiles(filenames...))
}

//-----------------------------------------------------------------------------
// Handlers

func rootHandler(c appengine.Context, w http.ResponseWriter, r *http.Request)(err error){
    if len(r.URL.Path) > 1 {
        return Err404
    }
    return blogPageView(c, w, 1, PostsPerPage)
}

func getSuffixName(base, full string) string {
    if len(full) == len(base) + 1 {
        return ""
    }
    return path.Clean(full)[len(base)+2:]
}

func blogHandler(c appengine.Context, w http.ResponseWriter, r *http.Request)(err error){
    idx, err := getPageIndex(r)
    if err != nil { return }
    permalink := getSuffixName("blog", r.URL.Path)
    if permalink == "" {
        // Redirect empty blog request to home page
        if idx == 1 {
            http.Redirect(w, r, "/", http.StatusFound);
            return
        }
        return blogPageView(c, w, int(idx), PostsPerPage)
    }
    // single blog
    // cache
    cacheKey := getCacheKey(c, permalink)
    if err = loadCache(c, w, cacheKey); err != ErrCacheMiss { return }
    // no cache
    p, err := getPost(c, permalink)
    if err != nil { return err }
    return layoutView(c, w, "blog", cacheKey, p)
}

// Get page index
func getPageIndex(r *http.Request) (int, error) {
    pageStr := r.URL.Query().Get("page")
    if pageStr != "" {
        idx64, err := strconv.ParseInt(pageStr, 10, 0)
        if err != nil { return 0, ErrInvalidPage }
        return int(idx64), nil
    }
    return 1, nil
}

func categoryPageHandler(c appengine.Context, w http.ResponseWriter, r *http.Request)(err error){
    idx, err := getPageIndex(r)
    if err != nil { return }

    // Get posts
    name := getSuffixName("category", r.URL.Path)
    if name == "" {
        http.Redirect(w, r, "/", http.StatusFound)
        return
    }
    // cache
    if err = loadCache(c, w, getPageCacheKey(c, "category/" + name, idx)); err != ErrCacheMiss { return }
    // no cache
    posts, total, err := getPostsByCategory(c, name, (idx-1) * PostsPerPage, PostsPerPage)
    if err != nil { return err }
    if total == 0 {
        return ErrCategoryNotExisted
    }
    return pageView(c, w, name + " - Category", "category/" + name, posts, idx, PostsPerPage, total)
}

func tagPageHandler(c appengine.Context, w http.ResponseWriter, r *http.Request)(err error){
    idx, err := getPageIndex(r)
    if err != nil { return }

    // Get posts
    name := getSuffixName("tag", r.URL.Path)
    if name == "" {
        http.Redirect(w, r, "/", http.StatusFound)
        return
    }
    // cache
    if err = loadCache(c, w, getPageCacheKey(c, "tag/" + name, idx)); err != ErrCacheMiss { return }
    // no cache
    posts, total, err := getPostsByTag(c, name, (idx-1) * PostsPerPage, PostsPerPage)
    if err != nil { return err }
    if total == 0 {
        return ErrTagNotExisted
    }
    return pageView(c, w, name + " - Tag", "tag/" + name, posts, idx, PostsPerPage, total)
}

func archivePageHandler(c appengine.Context, w http.ResponseWriter, r *http.Request)(err error){
    idx, err := getPageIndex(r)
    if err != nil { return }

    // Get posts
    name := getSuffixName("archive", r.URL.Path)
    if name == "" {
        http.Redirect(w, r, "/", http.StatusFound)
        return
    }
    date, err := time.Parse("Jan-2006", name)
    if err != nil { return ErrInvalidDate }
    // cache
    if err = loadCache(c, w, getPageCacheKey(c, "archive/" + name, idx)); err != ErrCacheMiss { return }
    // no cache
    posts, total, err := getPostsInMonth(c, date, (idx-1) * PostsPerPage, PostsPerPage)
    if err != nil { return err }
    if total == 0 {
        return ErrArchiveNotExisted
    }
    return pageView(c, w, date.Format("Jan 2006"), "archive/" + name, posts, idx, PostsPerPage, total)
}

func aboutHandler(c appengine.Context, w http.ResponseWriter, r *http.Request)(err error){
    return layoutView(c, w, "about", "about", nil)
}

//-----------------------------------------------------------------------------
// Helpers

// 1-based page index
func blogPageView(c appengine.Context, w http.ResponseWriter, curPage, postCount int) (err error) {
    // cache
    if err = loadCache(c, w, getPageCacheKey(c, "blog/", curPage)); err != ErrCacheMiss { return }
    // no cache
    // Get posts
    posts, total, err := getPostsInRange(c, (curPage-1) * postCount, postCount)
    if err != nil { return err }
    return pageView(c, w, "Home", "blog/", posts, curPage, postCount, total)
}

// 1-based index
func pageView(c appengine.Context, w http.ResponseWriter, title, urlpath string, posts []*Post, curPage, pageCount, total int) error {
    if total == 0 { return ErrEmptySite }

    total = (total - 1) / pageCount + 1
    if curPage > total || posts == nil {
        return ErrPageOutOfRange
    }

    // Generate page indices
    befores := make([]int, curPage - 1)
    afters := make([]int, total - curPage)
    for i, _ := range befores {
        befores[i] = i + 1
    }
    for i, _ := range afters {
        afters[i] = i + curPage + 1
    }
    nextpage := curPage + 1
    if nextpage > total {
        nextpage = 0
    }
    data := struct {
        Title       string
        UrlPath     string
        Posts       []*Post
        Previous    int
        Next        int
        CurrentPage int
        PagesBefore []int
        PagesAfter  []int
    }{
        title,
        urlpath,
        posts,
        curPage - 1,
        nextpage,
        curPage,
        befores,
        afters,
    }
    return layoutView(c, w, "index", getPageCacheKey(c, urlpath, curPage), &data)
}

// Layout view
func layoutView(c appengine.Context, w http.ResponseWriter, name, urlpath string, data interface{}) (err error) {
    view := &struct {
        IsAdmin bool
        Data    interface{}
    }{
        user.IsAdmin(c),
        data,
    }

    // Write to http ResponseWriter
    return createCacheForTemplate(c, w, urlpath, templates[name], view)
}

// Side view handler
func sideHandler(c appengine.Context, w http.ResponseWriter, r *http.Request)(err error){
    if !updateResponseHeader(w, r, getLastUpdatedTime(c)) { return }

    // Try from cache
    err = loadCache(c, w, "Side")
    if err != ErrCacheMiss { return }

    // Write side
    site, err := getSiteInfo(c)
    site.IsAdmin = user.IsAdmin(c)
    if err != nil { return err }
    err = createCacheForTemplate(c, w, "Side", templates["side"], site)
    return
}
