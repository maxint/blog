package lnyblog

import (
	"appengine"
	"appengine/datastore"
	"appengine/memcache"
	"net/url"
	"path"
	"regexp"
	"strings"
	"time"
)

type Blog struct {
	LastUpdatedAt time.Time
}

type Post struct {
	Header   *PostHeader // Separate header from blog for easy administration.
	Body     string
	Previous *PostHeader
	Next     *PostHeader
}

// URL: /post/litename
type PostHeader struct {
	PermaLink string // Unique, and unchangable after specified.
	Title     string
	CreatedAt time.Time
	UpdatedAt time.Time
	Category  string
	Tags      []string
	BodyKey   *datastore.Key
}

type PostBody struct {
	Content []byte
}

type SiteInfo struct {
	Archives   []*ArchiveInfo
	Categories map[string]int
	Tags       map[string]int
	IsAdmin    bool
}

type ArchiveInfo struct {
	DisplayName string
	LinkName    string
	Count       int
}

type HeaderCache struct {
	Headers []*PostHeader
}

var (
	regMoreSep      = regexp.MustCompile(MoreSep)
    localTimeZone   = time.FixedZone("UTC+8", 0)
)

//-----------------------------------------------------------------------------
// Helpers

func getKey(c appengine.Context, kind, key string) *datastore.Key {
	parent := datastore.NewKey(c, "Posts", "Posts", 0, nil)
	return datastore.NewKey(c, kind, key, 0, parent)
}

func bytesToString(b []byte) string {
	return string(b)
	// NOT work in appengine
	//bytesHeader := (*reflect.SliceHeader)(unsafe.Pointer(&b))
	//strHeader := reflect.StringHeader{bytesHeader.Data, bytesHeader.Len}
	//return *(*string)(unsafe.Pointer(&strHeader))
}

func getBlogInfoKey(c appengine.Context) *datastore.Key {
	return getKey(c, "Blog", "Blog Info")
}

func getLastUpdatedTime(c appengine.Context) time.Time {
	b := new(Blog)
	err := datastore.Get(c, getBlogInfoKey(c), b)
	if err == datastore.ErrNoSuchEntity {
		t := time.Now()
        datastore.Put(c, getBlogInfoKey(c), &Blog{LastUpdatedAt: t})
		return t
	}
	return b.LastUpdatedAt
}

//-----------------------------------------------------------------------------
// Other

func createPermaLink(date time.Time, title string) string {
	return date.Format("20060102-") + url.QueryEscape(strings.TrimSpace(title))
}

func createHeadersCache(c appengine.Context) (headers []*PostHeader, site *SiteInfo, err error) {
	// headers
	qall := datastore.NewQuery("Header").Order("-CreatedAt")
	total, err := qall.Count(c)
	if err != nil {
		return
	}

	c.Debugf("Creating cache for headers (N=%v) ...", total)
	headers = make([]*PostHeader, 0, total)
	_, err = qall.GetAll(c, &headers)
	if err != nil {
		return
	}

	// site info
	site = &SiteInfo{
		make([]*ArchiveInfo, 0),
		map[string]int{},
		map[string]int{},
		false,
	}
	var info *ArchiveInfo
	for _, h := range headers {
		dispName := h.CreatedAt.Format("Jan 2006")
		if info == nil || info.DisplayName != dispName {
			info = &ArchiveInfo{
				dispName,
				h.CreatedAt.Format("Jan-2006"),
				1,
			}
			site.Archives = append(site.Archives, info)
		} else {
			info.Count++
		}
		if h.Category == "" {
			h.Category = "default"
		}
		site.Categories[h.Category]++
		for _, t := range h.Tags {
			if t != "" {
				site.Tags[t]++
			}
		}
	}

	// Save result to memcahe
	err = memcache.Gob.Set(c, &memcache.Item{Key: "SiteInfo", Object: site})
	if err != nil {
		return
	}
	err = memcache.Gob.Set(c, &memcache.Item{Key: "HeaderCache", Object: &HeaderCache{headers}})
	if err != nil {
		return
	}

	c.Debugf("Creating cache for headers [DONE]")
	return
}

func getSiteInfo(c appengine.Context) (*SiteInfo, error) {
	site := new(SiteInfo)
	_, err := memcache.Gob.Get(c, "SiteInfo", site)
	if err == memcache.ErrCacheMiss {
		_, site, err = createHeadersCache(c)
	} else if err != nil {
		return nil, err
	}
	return site, err
}

func getAllHeaders(c appengine.Context) ([]*PostHeader, error) {
	hc := new(HeaderCache)
	_, err := memcache.Gob.Get(c, "HeaderCache", hc)
	if err == memcache.ErrCacheMiss {
		hc.Headers, _, err = createHeadersCache(c)
	} else if err != nil {
		return nil, err
	}
	return hc.Headers, err
}

//-----------------------------------------------------------------------------
// Post Operations

// Put post to datastore.
func putPost(c appengine.Context, header *PostHeader, body string) (err error) {
	// Generate keys
	phKey := getKey(c, "Header", header.PermaLink)
	pcKey := getKey(c, "Body", header.PermaLink)
	header.BodyKey = pcKey

	// Save post
	return datastore.RunInTransaction(c, func(c appengine.Context) error {
		if _, err := datastore.Put(c, phKey, header); err != nil {
			return err
		}
		if _, err := datastore.Put(c, pcKey, &PostBody{[]byte(body)}); err != nil {
			return err
		}
		if _, err := datastore.Put(c, getBlogInfoKey(c), &Blog{time.Now()}); err != nil {
			return err
		}
		if err := memcache.Flush(c); err != nil {
			return err
		}
		return nil
	}, nil)
}

// Delete post by url.
func deletePost(c appengine.Context, urlpath string) error {
	// Generate keys
	permalink := path.Base(urlpath)
	phKey := getKey(c, "Header", permalink)
	pcKey := getKey(c, "Body", permalink)

	// Delete post
	return datastore.RunInTransaction(c, func(c appengine.Context) error {
		if err := datastore.DeleteMulti(c, []*datastore.Key{phKey, pcKey}); err != nil {
			return err
		}
		if _, err := datastore.Put(c, getBlogInfoKey(c), &Blog{time.Now()}); err != nil {
			return err
		}
		if err := memcache.Flush(c); err != nil {
			return err
		}
		return nil
	}, nil)
}

func deleteAllPosts(c appengine.Context) (err error) {
    hall, err := getAllHeaders(c)
    if err != nil { return }

	// Get keys
	keys := make([]*datastore.Key, len(hall) * 2)
	for i, v := range hall {
		keys[2*i] = getKey(c, "Header", v.PermaLink)
		keys[2*i+1] = v.BodyKey
	}

	// Delete posts
	return datastore.RunInTransaction(c, func(c appengine.Context) error {
		if err := datastore.DeleteMulti(c, keys); err != nil {
			return err
		}
		if _, err := datastore.Put(c, getBlogInfoKey(c), &Blog{time.Now()}); err != nil {
			return err
		}
		if err := memcache.Flush(c); err != nil {
			return err
		}
		return nil
	}, nil)
}

// Get post from datastore by url.
func getPost(c appengine.Context, permalink string) (p *Post, err error) {
	if len(permalink) == 0 {
		return
	}
	// Get current header from cached headers
	hall, err := getAllHeaders(c)
	if err != nil || hall == nil {
		return
	}
	curIndex := -1
	for i, v := range hall {
		if v.PermaLink == permalink {
			curIndex = i
			break
		}
	}
	if curIndex == -1 {
		return nil, ErrBlogNotExisted
	}
	pheader := hall[curIndex]

	// Get body
	var pbody PostBody
	if err = datastore.Get(c, pheader.BodyKey, &pbody); err != nil {
		return
	}
	p = &Post{
		Header: pheader,
		Body:   bytesToString(pbody.Content),
	}

	// Previous and next post headers
	if curIndex > 0 {
		p.Next = hall[curIndex-1]
	}
	if curIndex < len(hall)-1 {
		p.Previous = hall[curIndex+1]
	}
	return
}

//-----------------------------------------------------------------------------
// Get header and post

func getPostsByHeaders(c appengine.Context, headers []*PostHeader, trimMore bool) (posts []*Post, err error) {
	count := len(headers)
	if count == 0 {
		return
	}

	// Get keys
	keys := make([]*datastore.Key, count)
	for i, v := range headers {
		keys[i] = v.BodyKey
	}

	// Get bodys
	bodys := make([]PostBody, count)
	if err = datastore.GetMulti(c, keys, bodys); err != nil {
		return
	}

	//Combine posts
	posts = make([]*Post, count)
	for i := 0; i < count; i++ {
		var b string
        filled := false
		if trimMore {
			loc := regMoreSep.FindIndex(bodys[i].Content)
			if loc != nil {
				b = bytesToString(bodys[i].Content[0:loc[0]])
                filled = true
			}
        }
        if !filled && TrimPreview {
            rs := []rune(string(bodys[i].Content))
            end := len(rs)
            if end > PreviewMaxChar {
                end = PreviewMaxChar
            }
            b = string(rs[0:end]) + " ..."
            filled = true
        }
        if !filled {
            b = bytesToString(bodys[i].Content)
        }
		posts[i] = &Post{
			Header: headers[i],
			Body:   b,
		}
	}
	return
}

func getIndexInRange(offset, count, total int) (int, int) {
	if offset < 0 {
		offset = 0
	}
	maxcount := total - offset
	if count < 0 {
		count = maxcount
	}
	if count > maxcount {
		count = maxcount
	}
	return offset, count
}

func getPostsByHeadersInRange(c appengine.Context, headers []*PostHeader, offset, count int) (posts []*Post, total int, err error) {
	total = len(headers)
	offset, count = getIndexInRange(offset, count, total)
	if count <= 0 {
		return
	}
	posts, err = getPostsByHeaders(c, headers[offset:offset+count], TrimAfterMore)
	return
}

// Get posts in the specified range, ordered by date.
func getPostsInRange(c appengine.Context, offset, count int) (posts []*Post, total int, err error) {
	headers, total, err := getHeadersInRange(c, offset, count)
	if err != nil {
		return
	}
	posts, err = getPostsByHeaders(c, headers, TrimAfterMore)
	return
}

// Get full content of all posts, for xmlsaver and feed.
func getAllPosts(c appengine.Context) (posts []*Post, err error) {
	hall, err := getAllHeaders(c)
	if err != nil || hall == nil {
		return
	}
	return getPostsByHeaders(c, hall, false)
}

func getLastestNPosts(c appengine.Context, num int) (posts []*Post, err error) {
	if num <= 0 {
		return
	}
	hall, err := getAllHeaders(c)
	if err != nil || hall == nil {
		return
	}
	total := len(hall)
	if num > total {
		num = total
	}
	return getPostsByHeaders(c, hall[0:num], false)
}

// Get post headers in the specified range, ordered by date.
func getHeadersInRange(c appengine.Context, offset, count int) (headers []*PostHeader, total int, err error) {
	if count == 0 {
		return
	}
	hall, err := getAllHeaders(c)
	if err != nil || hall == nil {
		return
	}
	total = len(hall)
	offset, count = getIndexInRange(offset, count, total)
	if count <= 0 {
		return
	}
	headers = hall[offset : offset+count]
	return
}

// By category
func getPostsByCategory(c appengine.Context, cat string, offset, count int) (posts []*Post, total int, err error) {
	hall, err := getAllHeaders(c)
	if err != nil || hall == nil {
		return
	}
	total = len(hall)
	hres := make([]*PostHeader, 0, total)
	for _, v := range hall {
		if v.Category == cat {
			hres = append(hres, v)
		}
	}
	return getPostsByHeadersInRange(c, hres, offset, count)
}

// By tags
func getPostsByTag(c appengine.Context, tag string, offset, count int) (posts []*Post, total int, err error) {
	hall, err := getAllHeaders(c)
	if err != nil || hall == nil {
		return
	}
	total = len(hall)
	hres := make([]*PostHeader, 0, total)
	for _, v := range hall {
		for _, t := range v.Tags {
			if t == tag {
				hres = append(hres, v)
				break
			}
		}
	}
	return getPostsByHeadersInRange(c, hres, offset, count)
}

// In month
func getPostsInMonth(c appengine.Context, date time.Time, offset, count int) (posts []*Post, total int, err error) {
	hall, err := getAllHeaders(c)
	if err != nil || hall == nil {
		return
	}
	total = len(hall)
	hres := make([]*PostHeader, 0, total)
	m := date.Month()
	y := date.Year()
	for _, v := range hall {
		if v.CreatedAt.Month() == m && v.CreatedAt.Year() == y {
			hres = append(hres, v)
		}
	}
	return getPostsByHeadersInRange(c, hres, offset, count)
}
