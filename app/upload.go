package lnyblog

import (
	"appengine"
	"appengine/blobstore"
	"appengine/datastore"
	"appengine/image"
	"encoding/json"
	"io"
	"mime"
	"net/http"
	"path"
	"strings"
	"time"
)

var (
	extTable = map[string][]string{
		"image": {"gif", "jpg", "jpeg", "png", "bmp"},
		"flash": {"swf", "flv"},
		"media": {"mp4", "ogg", "mov", "qt", "swf", "flv", "mp3", "wav", "wma", "wmv", "mid", "avi", "mpg", "asf", "rm", "rmvb"},
		"file":  {"doc", "docx", "xls", "xlsx", "ppt", "htm", "html", "txt", "zip", "rar", "gz", "bz2"},
	}
	maxSize = 1 << 20
)

// "FIle" kind
type FileInfo struct {
	Dir        string
	Name       string
	Size       int64
	Key        appengine.BlobKey
	UploadedAt time.Time
	ImageURL   string
}

func uploadHandler(c appengine.Context, w http.ResponseWriter, r *http.Request) (err error) {
	// Check major type
	dirName := r.FormValue("dir")
	if dirName == "" {
		dirName = "image"
	}
	exts, ok := extTable[dirName]
	if !ok {
		return uploadError(c, w, "Upload dir "+dirName+" does not existed!")
	}

	// Get file info
	file, fheader, err := r.FormFile("imgFile")
	if err != nil { return }

	// Check file extension
	ext := path.Ext(fheader.Filename)
	ok = false
	if len(ext) > 0 {
		for _, v := range exts {
			if ext[1:] == v {
				ok = true
				break
			}
		}
	}
	if !ok {
		return uploadError(c, w, "Upload file type "+ext+" is not allowed.\nAllowed file types contain "+strings.Join(exts, ", ")+".")
	}

	// Create blob
	blobw, err := blobstore.Create(c, mime.TypeByExtension(ext))
	if err != nil { return }

	// Write
	sz, err := io.Copy(blobw, file)
	if err != nil { return }

	// Save and get blob key
	err = blobw.Close()
	if err != nil { return }
	k, err := blobw.Key()
	if err != nil { return }

	// Get image serving URL
	var imageurl string
	if dirName == "image" {
		url, err := image.ServingURL(c, k, nil)
		if err != nil { return err }
		imageurl = url.Path
	}

	// Save file info to datastore
	date := time.Now()
	info := &FileInfo{
		dirName,
		fheader.Filename,
		sz,
		k,
		date,
		imageurl,
	}
	dbk := date.Format("20060102-150405-") + fheader.Filename
	_, err = datastore.Put(c, datastore.NewKey(c, "Files", dbk, 0, nil), info)
	if err != nil {
		err1 := blobstore.Delete(c, k)
		if err1 != nil {
			return err1
		}
		return
	}
	c.Debugf("Upload file: %v", fheader.Filename)

	// Return successful JSON
	//if imageurl != "" {
		//return uploadSuccess(c, w, imageurl)
	//}
	return uploadSuccess(c, w, "/files/"+string(dbk))
}

func uploadError(c appengine.Context, w http.ResponseWriter, msg string) (err error) {
	c.Warningf("Upload error: %v", msg)
	buf, err := json.Marshal(map[string]interface{}{
		"error":   1,
		"message": msg,
	})
	if err != nil { return }
	_, err = w.Write(buf)
	return nil //errors.New(msg)
}

func uploadSuccess(c appengine.Context, w http.ResponseWriter, url string) (err error) {
	buf, err := json.Marshal(map[string]interface{}{
		"error": 0,
		"url":   url,
	})
	if err != nil {
		return
	}
	_, err = w.Write(buf)
	return
}

func fileServeHandler(c appengine.Context, w http.ResponseWriter, r *http.Request) (err error) {
	key := path.Base(r.URL.Path)
	var info FileInfo
	err = datastore.Get(c, datastore.NewKey(c, "Files", key, 0, nil), &info)
	if err != nil {
		return ErrFileNotExisted
	}
    if updateResponseHeader(w, r, info.UploadedAt) {
        blobstore.Send(w, info.Key)
    }
	return
}
