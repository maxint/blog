package lnyblog

const (
    AtomPostCount   = 20
	MoreSep         = `<!--more-->`
	TrimAfterMore   = true
    TrimPreview     = false // WARNING: can not handle html tag correctly.
    PreviewMaxChar  = 200
    PostsPerPage    = 4     // Max number of posts in one page
    ThemeName       = "default"
    EditorTimeLayout= "2006-01-02T15:04:05Z07:00"
)
