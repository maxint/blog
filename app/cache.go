package lnyblog

import (
    "appengine"
    "appengine/memcache"
    "appengine/user"
    "bytes"
    "fmt"
    "io"
    "net/http"
    "time"
)

var (
    ErrCacheMiss = memcache.ErrCacheMiss
)

func getCacheKey(c appengine.Context, origkey string) string {
    return fmt.Sprintf("%v_%v", origkey, user.IsAdmin(c))
}

func getPageCacheKey(c appengine.Context, urlpath string, pageIndex int) string {
    return fmt.Sprintf("%v?page=%v_%v", urlpath, pageIndex, user.IsAdmin(c))
}

func loadCache(c appengine.Context, w io.Writer, key string) (err error) {
    item, err := memcache.Get(c, key)
    if err == nil {
        _, err = w.Write(item.Value)
    }
    return err
}

type Template interface {
    Execute(wr io.Writer, data interface{}) (err error)
}

// Create cache and write template to w
func createCacheForTemplate(c appengine.Context, w io.Writer, key string, tpl Template, data interface{}) (err error) {
    var buf bytes.Buffer
    if err = tpl.Execute(&buf, data); err != nil { return }
    if _, err = w.Write(buf.Bytes()); err != nil { return }

    // Create page cache
    return createCache(c, key, buf.Bytes())
}

// Create cache
func createCache(c appengine.Context, key string, buf []byte) (err error) {
    c.Debugf("Creating cache for %v ...", key)
    if err = memcache.Set(c, &memcache.Item{Key: key, Value: buf}); err != nil {
        c.Errorf("Creating cache for %v [FAILED] : %v", key, err)
        return
    }
    c.Debugf("Creating cache for %v [DONE]", key)
    return
}

// Return true if updating is needed.
func updateResponseHeader(w http.ResponseWriter, r *http.Request, lastmod time.Time) bool {
    str := r.Header.Get("If-Modified-Since")
    modsince, err := time.Parse(time.RFC1123Z, str)
    if err == nil && modsince.Unix() >= lastmod.Unix() {
        w.WriteHeader(304)
        return false
    }
    // Write headers
    w.Header().Set("Cache-Control", "private, max-age=3153600")
    w.Header().Set("Last-Modified", lastmod.Format(time.RFC1123))
    w.Header().Set("Expires", lastmod.AddDate(1, 0, 0).Format(time.RFC1123))
    return true
}
