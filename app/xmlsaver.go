package lnyblog

import (
    "appengine"
    "encoding/xml"
    "io"
    "io/ioutil"
    "log"
    "os"
    "path"
)

var (
    dataDir = "_data/"
)

func saveAllPostsToXML(c appengine.Context) (err error) {
    defer func() { if err != nil { log.Panicln(err) } }()
    if err = os.Mkdir(dataDir, os.ModeDir); err != nil && !os.IsExist(err) { return }
    posts, err := getAllPosts(c)
    if err != nil { return }
    for _, p := range posts {
        if err = p.saveToXML(dataDir); err != nil { return }
    }
    return
}

func loadAllPostsFromXML(c appengine.Context) (err error) {
    defer func() { if err != nil { log.Panicln(err) } }()
    dir, err := os.Open(dataDir)
    if err != nil { return }
    defer dir.Close()
    names, err := dir.Readdirnames(-1)
    if err != nil { return }
    for _, v := range names {
        p := new(Post)
        err = p.loadFromXML(dataDir + v)
        if err != nil { return }
        if err = putPost(c, p.Header, p.Body); err != nil { return }
    }
    return
}

func (p *Post) saveToXML(dir string) (err error) {
    defer func() { if err != nil { log.Panicln(err) } }()
    xmlFile, err := os.Create(path.Join(dir, p.Header.PermaLink + ".xml"))
    if err != nil { return }
    defer xmlFile.Close()

    return p.writeXML(xmlFile)
}

func (p *Post) loadFromXML(filepath string) (err error) {
    defer func() { if err != nil { log.Panicln(err) } }()
    xmlFile, err := os.Open(filepath)
    if err != nil { return }
    defer xmlFile.Close()

    return p.readXML(xmlFile)
}

func (p *Post) writeXML(w io.Writer) (err error) {
    buf, err := xml.MarshalIndent(p, "", "  ")
    if err != nil { return }
    _, err = w.Write(buf)
    return
}

func (p *Post) readXML(r io.Reader) (err error) {
    buf, err := ioutil.ReadAll(r)
    if err != nil { return }
    return xml.Unmarshal(buf, p)
}
