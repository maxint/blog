/*******************************************************************************
* KindEditor - WYSIWYG HTML Editor for Internet
* Copyright (C) 2006-2011 kindsoft.net
*
* @author Roddy <luolonghao@gmail.com>
* @site http://www.kindsoft.net/
* @licence http://www.kindsoft.net/license.php
*******************************************************************************/

// http://softwaremaniacs.org/soft/highlight/en/

KindEditor.plugin('code', function(K) {
	var self = this, name = 'code';
	self.clickToolbar(name, function() {
		var lang = self.lang(name + '.'),
			html = ['<div style="padding:10px 20px;">',
				'<div class="ke-dialog-row">',
				'<select class="ke-code-type">',
				'<option value="cpp">C++</option>',
				'<option value="cs">C#</option>',
				'<option value="bash">Bash</option>',
				'<option value="dos">DOS .bat</option>',
				'<option value="json">JSON</option>',
				'<option value="glsl">GLSL</option>',
				'<option value="go">Go</option>',
				'<option value="py">Python</option>',
				'<option value="matlab">Matlab</option>',
				'<option value="cmake">CMake</option>',
				'<option value="ini">Ini</option>',
				'<option value="js">JavaScript</option>',
				'<option value="html">HTML/XML</option>',
				'<option value="css">CSS</option>',
				'<option value="">Other</option>',
				'</select>',
				'</div>',
				'<textarea class="ke-textarea" style="width:408px;height:260px;"></textarea>',
				'</div>'].join(''),
			dialog = self.createDialog({
				name : name,
				width : 450,
				title : self.lang(name),
				body : html,
				yesBtn : {
					name : self.lang('yes'),
					click : function(e) {
						var type = K('.ke-code-type', dialog.div).val(),
							code = textarea.val(),
							html = '<pre><code class="' + type + '">\n' + K.escape(code) + '</code></pre> ';
						self.insertHtml(html).hideDialog().focus();
					}
				}
			}),
			textarea = K('textarea', dialog.div);
		textarea[0].focus();
	});
});
